package edu.psu.bd.cs.mth5416.country_ui.main;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Country_UI extends Application {

    DBHandler dbh;

    int showingIndex;

    Label codeSearchLabel, nameSearchLabel;
    TextField codeSearchArea, nameSearchArea;
    Button searchButton;
    Label nameTitleLabel, codeTitleLabel, headOfStateTitleLabel, languageTitleLabel, cityTitleLabel;
    TextField nameArea, codeArea, headOfStateArea;
    Label languageLabel, cityLabel;
    Button nextCountry, prevCountry;

    public void start(Stage primaryStage) throws Exception {

        codeSearchLabel = new Label("Country Codes");
        codeSearchArea = new TextField();
        HBox codeSearch = new HBox(codeSearchLabel, codeSearchArea);

        nameSearchLabel = new Label("Country Names");
        nameSearchArea = new TextField();
        HBox nameSearch = new HBox(nameSearchLabel, nameSearchArea);

        VBox searchArea = new VBox(codeSearch, nameSearch);
        searchButton =  new Button("Search");
        HBox search = new HBox(searchArea, searchButton);
        search.setAlignment(Pos.CENTER);

        nextCountry = new Button("Next");
        prevCountry = new Button("Previous");
        HBox prevNext = new HBox(prevCountry, nextCountry);
        prevNext.setAlignment(Pos.CENTER);
        prevNext.setSpacing(20);

        nameTitleLabel = new Label("Name: ");
        nameArea = new TextField();
        HBox name = new HBox(nameTitleLabel, nameArea);

        codeTitleLabel = new Label("Code: ");
        codeArea = new TextField();
        HBox code = new HBox(codeTitleLabel, codeArea);

        headOfStateTitleLabel = new Label("Head of State: ");
        headOfStateArea = new TextField();
        HBox headOfState = new HBox(headOfStateTitleLabel, headOfStateArea);

        languageTitleLabel = new Label("Language(s): ");
        languageLabel = new Label();
        HBox language = new HBox(languageTitleLabel, languageLabel);

        cityTitleLabel = new Label("Cities: ");
        cityLabel = new Label();
        HBox city = new HBox(cityTitleLabel, cityLabel);

        VBox resultBox = new VBox(name, code, headOfState, language, city);

        VBox everything = new VBox(search, resultBox);

        Scene scene = new Scene(everything);

        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }


}
