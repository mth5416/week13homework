package edu.psu.bd.cs.mth5416.country_ui.main;

import java.sql.*;
import java.util.Vector;

public class DBHandler {
    Connection c;

    public DBHandler(){
        try{
            c = DriverManager.getConnection("jdbc:sqlite:/Users/mason/Desktop/CMPSC221/country_ui/src/main/resources/World");
        } catch (SQLException s){
            s.printStackTrace();;
        }
    }

    public Vector<String> getCodesContaining(String codeSnippet){
        Vector<String> codes = new Vector<String>();
        try{
            String sql = "SELECT Code FROM Country WHERE Code LIKE ? ORDER BY Name ASC";
            PreparedStatement p = c.prepareStatement(sql);
            p.setString(1, "%" + codeSnippet + "%");
            ResultSet r = p.executeQuery();
            while(r.next()){
                codes.add(r.getString(1));
            }
        } catch (SQLException s){
            s.printStackTrace();
        }
        return codes;
    }

    public String getNameFromCode(String code){
        try{
            String sql = "SELECT Name FROM Country WHERE Code = ?";
            PreparedStatement p = c.prepareStatement(sql);
            p.setString(1, code);
            ResultSet r = p.executeQuery();
            return r.getString(1);
        } catch (SQLException s){
            s.printStackTrace();
        }
        return "";
    }

    public String getHeadOfStateFromCode(String code){
        try{
            String sql = "SELECT HeadOfState FROM Country WHERE Code = ?";
            PreparedStatement p = c.prepareStatement(sql);
            p.setString(1, code);
            ResultSet r = p.executeQuery();
            return r.getString(1);
        } catch (SQLException s){
            s.printStackTrace();
        }
        return "";
    }

    public Vector<String> getLanguagesFromCode(String code){
        Vector<String> languages = new Vector<String>();
        try{
            String sql = "SELECT Language FROM CountryLanguage WHERE CountryCode = ?";
            PreparedStatement p = c.prepareStatement(sql);
            p.setString(1, code);
            ResultSet r = p.executeQuery();
            while(r.next()){
                languages.add(r.getString(1));
            }
            return languages;
        } catch (SQLException s){
            s.printStackTrace();
        }
        return languages;
    }

    public Vector<String> getCitiesFromCode(String code){
        Vector<String> cities = new Vector<String>();
        try{
            String sql = "SELECT Name FROM City WHERE CountryCode = ? ORDER BY Name ASC";
            PreparedStatement p = c.prepareStatement(sql);
            p.setString(1, code);
            ResultSet r = p.executeQuery();
            while(r.next()){
                cities.add(r.getString(1));
            }
            return cities;
        } catch (SQLException s){
            s.printStackTrace();
        }
        return cities;
    }

    public Vector<String> getNamesContaining(String nameSnippet){
        Vector<String> names = new Vector<String>();
        try{
            String sql = "SELECT Name from Country WHERE Name LIKE ? ORDER BY Name ASC";
            PreparedStatement p = c.prepareStatement(sql);
            p.setString(1, "%" + nameSnippet + "%");
            ResultSet r = p.executeQuery();
            while(r.next()){
                names.add(r.getString(1));
            }
        } catch (SQLException s){
            s.printStackTrace();
        }
        return names;
    }

    public String getCodeFromName(String name){
        try{
            String sql = "SELECT Code from Country WHERE Name = ?";
            PreparedStatement p = c.prepareStatement(sql);
            p.setString(1, name);
            ResultSet r = p.executeQuery();
            return r.getString(1);
        } catch (SQLException s){
            s.printStackTrace();
        }
        return "";
    }

    public String getHeadOfStateFromName(String name){
        return getHeadOfStateFromCode(getCodeFromName(name));
    }

    public Vector<String> getLanguagesFromName(String name){
        return getLanguagesFromCode(getCodeFromName(name));
    }

    public Vector<String> getCitiesFromName(String name){
        return getCitiesFromCode(getCodeFromName(name));
    }

    public void close(){
        try {
            c.close();
        } catch (SQLException s){
            s.printStackTrace();
        }
    }

}
