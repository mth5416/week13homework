package edu.psu.bd.cs.mth5416.country_ui.main;

import java.sql.*;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Vector;

public class CountryTest{
    public static void main(String[] args) {
        Connection c;
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a country code: ");
        String code = scan.nextLine();

        DBHandler dbh = new DBHandler();
        Vector<String> codes = dbh.getCodesContaining(code);
        Iterator<String> iter = codes.iterator();
        Country tempCountry;
        while(iter.hasNext()){
            tempCountry = new Country(iter.next());
            System.out.println(tempCountry);
            tempCountry.close();
        }

        System.out.println("\nEnter a country name: ");
        String name = scan.nextLine();
        Vector<String> names = dbh.getNamesContaining(name);
        iter = names.iterator();
        while (iter.hasNext()) {
            tempCountry = new Country(dbh.getCodeFromName(iter.next()));
            System.out.println(tempCountry);
            tempCountry.close();
        }

        dbh.close();
    }
}